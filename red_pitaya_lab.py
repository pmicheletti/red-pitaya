# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 18:45:27 2021

@author: polen
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May  4 11:59:43 2021

@author: polen
"""

# pythonprogramminglanguage.com
import sys
import numpy as np
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox,
                             QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget, QSlider, QLabel, QDoubleSpinBox,QMainWindow)

from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas


from scipy.constants import e, hbar, h
sys.path.append(r'\\windata.ethz.ch\mesoqc\lab\meas\Paolo\Python\RedPitaya')
import redpitaya_class 

#%%
            



class KS_app(QWidget):
    
    
    def __init__(self, parent=None):
        super(KS_app, self).__init__(parent)
        
        self.rdpt = redpitaya_class.redpitaya_scope()        
        self.init_instr()
        
        outerLayout = QVBoxLayout()
        plotLayout = QVBoxLayout()
        button_layout = QVBoxLayout()

        self.button = QPushButton("START")
        self.button.clicked.connect(self.strt_stp)
        button_layout.addWidget(self.button)
        self.canvas = FigureCanvas(plt.Figure(figsize=(8, 6)))
        plotLayout.addWidget(self.canvas)
        self.init_plot() 
        
        
        grid = QGridLayout()
        grid.addWidget(self.slider([self.tau3, self.tau_par],self.update_plot, title = 'lifetimes'), 0, 0)
        grid.addWidget(self.slider([self.Omega3i], self.update_plot), 0, 2)  
        grid.addWidget(self.spinbox([self.A, self.ns ,self.Np, self.di3], self.reload, title = 'others'), 0, 3)
        grid.addWidget(self.spinbox([self.alphaw, self.R1, self.R2, self.eta], self.reload, title = 'fit parameters'), 0, 4)
        
        outerLayout.addLayout(button_layout)
        outerLayout.addLayout(plotLayout)
        outerLayout.addLayout(grid)
               
        self.dialogs = list()
        
        self.setLayout(outerLayout)

    def slider(self, var_list, action,  title = ''):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
            sld = QSlider(Qt.Horizontal)
            sld.setFocusPolicy(Qt.StrongFocus)
            sld.setTickPosition(QSlider.TicksBothSides)
            sld.setTickInterval(10)
            sld.setSingleStep(1)
            sld.setValue(33)
            sld.valueChanged[int].connect(var.update_slider)
            sld.valueChanged[int].connect(action)
        
            label =QLabel()
            label.setText(var.name)
            Ggrid = QGridLayout()
            Ggrid.addWidget(var.num_lbl, 0,1)
            Ggrid.addWidget(label, 0,0)
            vbox.addLayout(Ggrid)
            vbox.addWidget(sld)

        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox
    
    def spinbox(self, var_list, action, title = ''):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
        
            Ggrid = QGridLayout()
            spinbox = QDoubleSpinBox()
            spinbox.setFixedSize(100,20)
            spinbox.setSuffix(var.unit)
            spinbox.setDecimals(3)
            spinbox.setRange(var.val*var.fact/100,var.val*var.fact*100)
            spinbox.setSingleStep(var.val*var.fact/20)
            spinbox.setValue(var.val*var.fact)
            spinbox.valueChanged[float].connect(var.update_spinbox)
            spinbox.valueChanged.connect(action)
            label =QLabel()
            label.setText(var.name)
            Ggrid.addWidget(spinbox, 0, 1)
            Ggrid.addWidget(label, 0, 0)
            vbox.addLayout(Ggrid)
            
        
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox

    def strt_stp(self):
        if self.button.
    
    def init_plot(self):
        
        font = {
            'weight': 'normal',
            'size': 14
        }
        plt.rc('font', **font)

        self.ax = self.canvas.figure.subplots()
        self.ax.set_xlabel('$\Delta$ (meV)')
        self.ax.set_ylabel('R$_d$ ($\Omega$)', color = 'tab:blue')
        self.ax.tick_params(axis='y', labelcolor = 'tab:blue')
        self.ax2=self.ax.twinx()
        
        self.ax2.set_ylabel('R$_{nr}$ ($\Omega$)', color = 'tab:orange')
        self.ax2.tick_params(axis='y', labelcolor = 'tab:orange')
        self.line_Rd, = self.ax.plot([], [],'*', color = 'tab:blue')
        self.line_Rnr, = self.ax2.plot([], [],'*', color = 'tab:orange')
        self.line_Rdft, = self.ax.plot([], [], color = 'tab:blue')
        self.line_Rnrft, = self.ax2.plot([], [], color = 'tab:orange', label = 'R$_{nr}$')
        self.line_Rnrft_Rser, = self.ax2.plot([], [], '--', color = 'tab:orange', label = 'R$_{nr}$ + R$_{series}$ (22 $\Omega$)')
        self.ax2.legend()
        self.update_plot()
       
    def update_plot(self):
        
        Rd_ft = self.Rd_func()
        Rnr_ft = self.Rnr_func()
        self.ax.set_xlim([min(self.Del_fit), max(self.Del_fit)])
        self.ax2.set_xlim([min(self.Del_fit), max(self.Del_fit)])
        self.ax.set_ylim([ min( min(Rd_ft ), min(self.Rd)*0.9 ), max(max(Rd_ft), max(self.Rd)*1.1 )])
        self.ax2.set_ylim([ min( min(Rnr_ft ), min(self.Rnr)*0.9 ), max(max(Rnr_ft), max(self.Rnr)*1.1 )])
        
        self.line_Rd.set_data(self.Delta, self.Rd)
        self.line_Rnr.set_data(self.Delta, self.Rnr)
        self.line_Rdft.set_data(self.Del_fit, Rd_ft)
        self.line_Rnrft.set_data(self.Del_fit, Rnr_ft)
        self.line_Rnrft_Rser.set_data(self.Del_fit, Rnr_ft+22)
        self.ax.set_title('4|$\Omega_{i3}|^{2} \u03C4_{||}\u03C4_3$ = %3.2f, $\eta_{opt}$ = %3.2f'%( 4*(self.Omega3i.val*1e-3/hbar*e)**2*self.tau3.val*self.tau_par.val, self.eta_opt*1e2  )+'%' )
        self.canvas.draw()


                        
    def init_var(self):
                
        self.dec =1,                 #decimation
        self.avg = 'ON',             #averaging
        self.trig_src = 'EXT_PE',    #trigger source (external to default)    
        self.trig_delay = 0,         #trigger delay (ns)
        self.trig_lev = 1000,        #trigger level (mV)        
        self.rdpt.set_scope(dec = self.dec, avg = self.avg, trig_src = self.trig_src, trig_delay = self.trig_delay, trig_lev = self.trig_lev )
  
    
            


if __name__ == '__main__':
    app = QApplication(sys.argv)
    myApp = KS_app()
    myApp.show()
    sys.exit(app.exec_())