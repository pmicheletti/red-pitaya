# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 17:14:59 2021

@author: polen
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May  4 11:59:43 2021

@author: polen
"""

# pythonprogramminglanguage.com
import time
import sys
import numpy as np
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox,
                             QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget, QSlider, QLabel, QDoubleSpinBox,QMainWindow)


from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas


from scipy.constants import e, hbar, h
sys.path.append(r'\\windata.ethz.ch\mesoqc\lab\meas\Paolo\Python\Analysis')
from my_plots import nice_colors

sys.path.append(r'\\windata.ethz.ch\mesoqc\lab\meas\Paolo\Python\RedPitaya\RedPitaya-master\red_pitaya_streaming_client_python-master\red_pitaya_streaming_client_python-master')
from rp_stream_threaded import SocketClientThread, ClientCommand, ClientReply
sys.path.append(r'Z:\lab\meas\Paolo\Python\Analysis')
from math_functions import demod

#%%
class var():
    def __init__(self, label, v0, fact = 1, unit = '', scale = 1):
        
        self.fact = fact
        self.unit = unit
        self.name = label
        self.val0 = v0
        self.val = self.val0
        self.num_lbl =QLabel()
        self.scale = scale
        self.num_lbl.setText('= %3.2f %s'%(self.val*self.fact, self.unit))
        
    def update_slider(self, x):
        self.val = self.val0*(x/100*self.scale + (1-self.scale/2))
        self.num_lbl.setText('= %3.2f %s (%3.2f %s\u2080)'%(self.val*self.fact, self.unit, (x/100*self.scale + (1-self.scale/2)), self.name))
    def update_spinbox(self, x):
        self.val = x/self.fact
        self.num_lbl.setText('= %3.2f %s'%(self.val*self.fact, self.unit))
    def update_slider_add(self, x):
        self.val = self.val0+(x/100-0.5)*self.scale
        self.num_lbl.setText('= %3.2f %s '%(self.val*self.fact, self.unit))

           


class boxcar_lockin_app(QWidget):
    
    
    def __init__(self, parent=None):
        super(boxcar_lockin_app, self).__init__(parent)
        self.init_var()
        

        outerLayout = QVBoxLayout()
        plotLayout = QVBoxLayout()
        button_layout = QVBoxLayout()


        grid_top = QGridLayout()
               
        self.button = QPushButton("Start")
        self.button.setCheckable(True)
        self.button.clicked.connect(self.change_button)
        grid_top.addWidget(self.button, 0,0)
        
        self.label_speed = QLabel()
        grid_top.addWidget(self.label_speed, 0,1)
        self.label_it = QLabel()
        grid_top.addWidget(self.label_it, 0,2)
        self.label_fsam = QLabel()
        grid_top.addWidget(self.label_fsam, 0,3)


        
        
        self.canvas = FigureCanvas(plt.Figure(figsize=(10, 10)))
        plotLayout.addWidget(self.canvas)
        self.init_plot() 
        
        
        grid = QGridLayout()
        grid.addWidget(self.spinbox([self.f_sgn], self.reset_acquisition, title = 'master freq'), 1, 0)
        grid.addWidget(self.slider([self.trig_lev, self.integr_time], self.reset_acquisition, title = 'master trigger'), 0, 0)
        grid.addWidget(self.spinbox([ self.peak_width, self.trig_del], self.reset_acquisition, title = 'window control'), 1, 1)
        grid.addWidget(self.slider([self.trig_lev_sign], self.reset_acquisition, title = 'window trigger'), 0, 1)
        
#        sub_grid = QGridLayout()
        grid.addWidget(self.slider_simple(self.phase, self.update_lockin, title = 'phase', add = True), 0, 2)
        self.button_ph = QPushButton("Autophase")
        self.button_ph.clicked.connect(self.autophase)
        grid.addWidget(self.button_ph, 1, 2)
        
#        grid.addLayout(sub_grid, 0, 3)
        

        self.timer = QTimer()   #start a timer to get process data
        self.timer.timeout.connect(self.measure)
        #self.timer.setInterval(200)
        
        self.dataframe=0  #counter for number of data packets
        self.dataframeN=100
        self.LOST=0
        
        outerLayout.addLayout(grid_top)
        outerLayout.addLayout(button_layout)
        outerLayout.addLayout(plotLayout)
        outerLayout.addLayout(grid)
        
        self.dialogs = list()
        self.setLayout(outerLayout)
        
        self.QUEUE_DEPTH=1  
        self.SERVER_ADDR = '169.254.248.16', 8900
        self.client = SocketClientThread(self.QUEUE_DEPTH)
        self.client.start()
        #self.client.cmd_q.put(ClientCommand(ClientCommand.CONNECT, self.SERVER_ADDR))
               
        self.conv2 = 5.16/9575
        self.conv1 =  3.38/11000
        self.timeout = 1000

    def slider(self, var_list, action,  title = '', add = False):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
            sld = QSlider(Qt.Horizontal)
            sld.setFocusPolicy(Qt.StrongFocus)
            sld.setTickPosition(QSlider.TicksBothSides)
            sld.setTickInterval(10)
            sld.setSingleStep(1)
            sld.setValue(50)
            if add:
                sld.valueChanged[int].connect(var.update_slider_add)
            else:
                sld.valueChanged[int].connect(var.update_slider)
            sld.valueChanged[int].connect(action)
        
            label =QLabel()
            label.setText(var.name)
            Ggrid = QGridLayout()
            Ggrid.addWidget(var.num_lbl, 0,1)
            Ggrid.addWidget(label, 0,0)
            vbox.addLayout(Ggrid)
            vbox.addWidget(sld)

        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox
    
    def spinbox(self, var_list, action, title = ''):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
        
            Ggrid = QGridLayout()
            spinbox = QDoubleSpinBox()
            spinbox.setFixedSize(100,20)
            spinbox.setSuffix(var.unit)
            spinbox.setDecimals(3)
            spinbox.setRange(-np.sign(var.val)*var.val*var.fact*100,np.sign(var.val)*var.val*var.fact*100)
            spinbox.setSingleStep(var.val*var.fact/10)
            spinbox.setValue(var.val*var.fact)
            spinbox.valueChanged[float].connect(var.update_spinbox)
            spinbox.valueChanged.connect(action)
            label =QLabel()
            label.setText(var.name)
            Ggrid.addWidget(spinbox, 0, 1)
            Ggrid.addWidget(label, 0, 0)
            vbox.addLayout(Ggrid)
            
        
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox

    def slider_simple(self, var, action,  title = '', add = False):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
                    
        self.sld = QSlider(Qt.Horizontal)
        self.sld.setFocusPolicy(Qt.StrongFocus)
        self.sld.setTickPosition(QSlider.TicksBothSides)
        self.sld.setTickInterval(10)
        self.sld.setSingleStep(1)
        self.sld.setValue(50)
        self.sld.valueChanged[int].connect(var.update_slider_add)
        
        label =QLabel()
        label.setText(var.name)
        Ggrid = QGridLayout()
        Ggrid.addWidget(var.num_lbl, 0,1)
        Ggrid.addWidget(label, 0,0)
        vbox.addLayout(Ggrid)
        vbox.addWidget(self.sld)
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox    
    
    
    def init_plot(self):
        
        font = {
            'weight': 'normal',
            'size': 11
        }
        plt.rc('font', **font)
        
        self.ax = self.canvas.figure.subplots(nrows=2, ncols=2, gridspec_kw={'width_ratios': [2.5, 1]})
        
        self.ax[0,0].set_xlabel('time (ms)')
        self.ax[0,0].set_ylabel('lock-in singnal (mV)')
        self.line_r, = self.ax[0,0].plot([],[], color = 'tab:blue', label = 'R')
        self.line_x, = self.ax[0,0].plot([],[], color = 'tab:orange', label = 'X')
        self.line_y, = self.ax[0,0].plot([],[], color = 'tab:green', label = 'Y')
        self.ax[0,0].legend()
        
        self.ax[0,1].set_ylabel('lock-in singnal (mV)')
        self.ax[0,1].set_yticks([0,1,2])
        self.ax[0,1].set_yticklabels(['Y','X','R'])
        self.ax[0,1].set_ylim([-0.5, 2.5])
        self.ax[0,1].plot(np.zeros(100),np.linspace(-0.5,2.5,100),'--', color = 'black')
        self.Rbar = self.ax[0,1].barh([2],[1], color = 'tab:blue', height = 0.2)
        self.Xbar = self.ax[0,1].barh([1],[1], color = 'tab:orange', height = 0.2)
        self.Ybar = self.ax[0,1].barh([0],[1], color = 'tab:green', height = 0.2)
        
        
        self.ax[1,0].set_ylabel('V')
        self.ax[1,0].set_xlabel('$\mu$s')
        self.line_sgn, = self.ax[1,0].plot([],[], label = 'Sign', color = 'tab:blue')
        self.line_ref, = self.ax[1,0].plot([],[], label = 'Ref', color = 'darkblue')
        self.ax[1,0].legend()
        
        self.ax[1,1].set_ylabel('V')
        self.ax[1,1].set_xlabel('ns')
        self.line_sgn_raw, = self.ax[1,1].plot([],[],'-*', label = 'Sign', color = 'tab:blue')
        self.line_trg1, = self.ax[1,1].plot([],[],'--', linewidth = 0.75, label = 'window', color = 'tab:red')
        self.line_trg2, = self.ax[1,1].plot([],[],'--', linewidth = 0.75, color = 'tab:red')
        self.line_trg_lev, = self.ax[1,1].plot([],[], linewidth = 1, label = 'trig level', color = 'darkblue')
        self.ax[1,1].legend()


       
    def update_lockin(self):
        
        if len(self.R)>0:
            t_lockin = np.arange(len(self.R))*self.size_ref/self.f_sampl
            self.line_r.set_data(t_lockin,self.R*1e3)
            self.line_x.set_data(t_lockin,self.X*1e3)
            self.line_y.set_data(t_lockin,self.Y*1e3)
            self.ax[0,0].set_xlim([0, t_lockin[-1]])
            self.ax[0,0].set_ylim([-max(self.R)*1e3, max(self.R)*1.1e3])
        
        self.Rbar.remove()
        self.Xbar.remove()
        self.Ybar.remove()

        self.Rbar = self.ax[0,1].barh([2],[self.R[-1]], color = 'tab:blue', height = 0.2)
        self.Xbar = self.ax[0,1].barh([1],[self.X[-1]], color = 'tab:orange', height = 0.2)
        self.Ybar = self.ax[0,1].barh([0],[self.Y[-1]], color = 'tab:green', height = 0.2)
        self.ax[0,1].set_xlim([-max(self.R)*1.1, max(self.R)*1.1])
        
        
        self.canvas.draw()
        plt.pause(0.01)
        
    def update_Data(self):   
        
        if self.size_sgn ==0 or self.size_ref ==0:
            return        
        t_sign = np.arange(self.size_sgn)/self.f_sgn.val*1e3
        self.line_sgn.set_data(t_sign, self.sign)  
        t_ref = np.arange(self.size_ref)/self.f_sampl*1e3
        self.line_ref.set_data(t_ref, self.ref*max(self.sign)) 
        self.ax[1,0].set_xlim([t_sign[0], t_sign[-1]])
        self.ax[1,0].set_ylim([0, max(self.sign)*1.1])
        
        if len(self.sign_rw)==0:
            return
        limy = max(self.sign_rw)*1.1
        limy_m = min(self.sign_rw)*0.95
        t_raw = (t_ref[:len(self.sign_rw)]-t_ref[int(self.T_trg)//2])*1e3
        self.line_sgn_raw.set_data( t_raw, self.sign_rw)
        self.line_trg1.set_data(np.ones(100)*self.trig_del.val*self.trig_del.fact, np.linspace(limy_m,limy,100))
        self.line_trg2.set_data(np.ones(100)*(self.trig_del.val+self.peak_width.val)*self.peak_width.fact, np.linspace(limy_m,limy,100))
        self.line_trg_lev.set_data(t_raw, np.ones(len(self.sign_rw))*self.trig_lev_sign.val )
        self.ax[1,1].set_xlim([t_raw[0], t_raw[-1]])
        self.ax[1,1].set_ylim([limy_m, limy])
        
        self.canvas.draw()
        plt.pause(0.01)


                        
    def init_var(self):
        
        self.f_sampl = 125e3; #kHz
        self.sign_raw =np.array([])
        self.ref_raw =np.array([])
        
        self.f_sgn = var('f sign', 800, unit = 'MHz', fact = 1e-3)  #kHz        
        self.trig_del = var('window delay', 10e-6, unit = 'ns', fact = 1e6) #ms
        self.peak_width = var('window width', 70e-6, unit = 'ns', fact = 1e6) #ms
        
        self.trig_lev_sign = var('gate trigger level', 4, unit = 'V', fact = 1) #ns
        self.trig_lev =var('main trigger level', 1, unit = 'V', fact = 1) #ns 
        
        self.integr_time=var('integration time', 100, unit = 'ms', fact = 1, scale = 50) #ms
        self.phase =var('phase', -np.pi/2 , unit = '\u03C0', fact = 1/np.pi, scale = 2*np.pi) 
        
        self.measure_en = False
    

        

    def reset_acquisition(self):
        self.measure_toggle()
        time.sleep(0.2)
        self.measure_toggle()
        
    
    def measure(self):
        self.sign = np.array([])
        self.ref = np.array([])
        
        self.t = 0
        self.err = 0
        while self.t < self.integr_time.val*self.f_sgn.val:
            
            a = self.getData()
            if a != 1:
                print('No data could be taken..')
                if self.err < self.timeout:
                    self.err += 1
                    continue
                else:
                    self.change_button()
                    break
            
            self.ref_raw = self.ref_raw >= self.trig_lev.val
            where_trg_ref = np.squeeze(np.argwhere(np.diff(self.ref_raw != 0)) )

            self.ref_raw = self.ref_raw[where_trg_ref[0]:where_trg_ref[(2*len(where_trg_ref)-1)//2]]
            self.sign_raw = self.sign_raw[where_trg_ref[0]:where_trg_ref[(2*len(where_trg_ref)-1)//2]]
            
            sign_trig = np.squeeze( np.argwhere( np.diff( np.array(self.sign_raw>self.trig_lev_sign.val, dtype = float) )>=1 ))+1
            self.T_trg = np.mean(np.diff(sign_trig))
            self.f_sampl = self.T_trg*self.f_sgn.val # number of samples/ms
            
            delay = int(self.f_sampl*self.trig_del.val)
            N = max(int(self.f_sampl*self.peak_width.val),1)
            for i in range(len(sign_trig)):
                self.sign = np.append(self.sign, np.mean(self.sign_raw[sign_trig[i]+delay:sign_trig[i]+delay+N]) )
                
            
            self.ref = np.concatenate((self.ref, self.ref_raw)) 
            if self.t ==0:
               self.size_sgn =  len(self.sign)
               self.size_ref =  len(self.ref)
               self.sign_rw = self.sign_raw[sign_trig[1]-int(self.T_trg)//2:sign_trig[2]]
               self.update_Data()
                
            self.t = self.t + len(self.sign)
            
        self.label_it.setText('Done in %d loops'%self.t)
        self.label_fsam.setText('Sample rate %3.2f MHz'%(self.f_sampl/1e3))
        
        if self.err < self.timeout:
            
            self.ref = self.ref_raw >= self.trig_lev.val
            r,x,y = demod(self.sign, self.ref, phi = self.phase.val)
            self.R, self.X, self.Y  = np.append(self.R, r), np.append(self.X, x), np.append(self.Y, y)
            self.update_lockin()    
            
    def measure_toggle(self):
        
        if self.measure_en:
            self.timer.stop()
            self.measure_en = False
        else:
              #time for frame rate
            self.timer.start(0)
            self.timestart=time.time()
            self.measure_en = True

    def  autophase(self):
        self.measure_toggle()
        self.phase.val = np.pi/2-np.arctan(self.Y[-1]/self.X[-1])
        if np.isnan(self.phase.val):
            self.phase.val =0
            
        self.sld.setValue(((self.phase.val-self.phase.val0)/(2*np.pi)+0.5)*100)
        self.phase.update_slider_add(0)
        
        self.measure_toggle()

            
    def change_button(self):
        
        if self.measure_en:
            self.button.setText('Start')
        else:
            self.button.setText('Stop')
            self.client.cmd_q.put(ClientCommand(ClientCommand.CONNECT, self.SERVER_ADDR))
            self.getData()
            self.R, self.X, self.Y  = np.array([]), np.array([]), np.array([])
            time.sleep(0.5)

        
        self.measure_toggle()
        
        
        
    def getData(self):
        if self.client.reply_q.qsize():
            a=self.client.reply_q.get()
            if a.type==0:  #ERROR
                print(a.data)
                print("ERROR: qsize ",self.client.reply_q.qsize())
                print('Resetting acquisition..')               
                self.err = self.timeout               
                
            if a.type==1:   #DATA
                self.dataframe+=1
                #t=a.data['params']['timestamp']               
                datat=a.data['bytes_data1']
                self.sign_raw = datat*self.conv1
                
                datat=a.data['bytes_data2']
                self.ref_raw=datat*self.conv2
                self.LOST+=a.data['params']['lostrate']
                #self.lcdNumber_lost.display(self.LOST)
                
                if self.dataframe==(self.dataframeN-1):
                    self.label_speed.setText('Acq rate: %3.2f Ms/s'%(2**16*self.dataframeN/(time.time()-self.timestart)/1024/1024))
                    self.timestart=time.time() #reset timer
                    self.dataframe=0
                    self.LOST=0
                    
            if a.type==2:  #MESSAGE
                print("MESSAGE: qsize ",self.client.reply_q.qsize())
                print(a.data)
            
            return a.type
        
        else:
            return 0

        
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    myApp = boxcar_lockin_app()
    myApp.resize(1050,900)
    myApp.show()
    sys.exit(app.exec_())