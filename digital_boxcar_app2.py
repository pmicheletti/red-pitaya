# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 17:14:59 2021

@author: polen
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May  4 11:59:43 2021

@author: polen
"""

# pythonprogramminglanguage.com
import sys
import numpy as np
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox,
                             QMenu, QPushButton, QRadioButton, QVBoxLayout, QWidget, QSlider, QLabel, QDoubleSpinBox,QMainWindow)

from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas


from scipy.constants import e, hbar, h
sys.path.append(r'\\windata.ethz.ch\mesoqc\lab\meas\Paolo\Python\Analysis')
from my_plots import nice_colors
sys.path.append(r'\\windata.ethz.ch\mesoqc\lab\meas\Paolo\Python\RedPitaya')
import redpitaya_class

#%%
class var():
    def __init__(self, label, v0, fact = 1, unit = ''):
        
        self.fact = fact
        self.unit = unit
        self.name = label
        self.val0 = v0
        self.val = self.val0
        self.num_lbl =QLabel()
        self.num_lbl.setText('= %3.2f %s'%(self.val*self.fact, self.unit))
        
    def update_slider(self, x):
        self.val = self.val0*(x/100*1.5+0.5)
        self.num_lbl.setText('= %3.2f %s (%3.2f %s\u2080)'%(self.val*self.fact, self.unit, (x/100*1.5+0.5), self.name))
    def update_spinbox(self, x):
        self.val = x/self.fact
        self.num_lbl.setText('= %3.2f %s'%(self.val*self.fact, self.unit))
    def update_slider_add(self, x, scale =np.pi):
        self.val = self.val0+(x/100-0.5)*scale
        self.num_lbl.setText('= %3.2f %s '%(self.val*self.fact, self.unit))

           


class boxcar_lockin_app(QWidget):
    
    
    def __init__(self, parent=None):
        super(boxcar_lockin_app, self).__init__(parent)
        self.init_var()
        self.rdpt = redpitaya_class.redpitaya(plots = False)
        

        outerLayout = QVBoxLayout()
        plotLayout = QVBoxLayout()
        
        grid_top = QGridLayout()
               
        self.button = QPushButton("Start")
        self.button.setCheckable(True)
        self.button.clicked.connect(self.change_button)
        grid_top.addWidget(self.button, 0,0)
        
        self.button_art = QPushButton("Generate signal")
        self.button_art.setCheckable(True)
        self.button_art.clicked.connect(self.generate)
        grid_top.addWidget(self.button_art, 0,1)

        
        
        self.canvas = FigureCanvas(plt.Figure(figsize=(10, 10)))
        plotLayout.addWidget(self.canvas)
        self.init_plot() 
        
        
        grid = QGridLayout()
        grid.addWidget(self.spinbox([self.f_sgn], self.reset_acquisition, title = 'master freq'), 0, 0)
        grid.addWidget(self.slider([self.trig_lev, self.trig_del], self.reset_acquisition, title = 'master trigger'), 1, 0)
        grid.addWidget(self.spinbox([ self.window_width, self.window_del], self.reset_acquisition, title = 'window control', one = True), 0, 1)
        grid.addWidget(self.slider([self.integr_time], self.update_plot, title = 'window trigger'), 1, 1)
        
#        sub_grid = QGridLayout()
        grid.addWidget(self.slider_simple(self.phase, self.update_plot, title = 'phase', add = True), 1, 2)
        self.button_ph = QPushButton("Autophase")
        self.button_ph.clicked.connect(self.autophase)
        grid.addWidget(self.button_ph, 0, 2)
        
#        grid.addLayout(sub_grid, 0, 3)
        
        self.timer = QTimer()
        self.timer.setInterval(200)
        self.timer.timeout.connect(self.measure)

        
        outerLayout.addLayout(grid_top)
        outerLayout.addLayout(plotLayout)
        outerLayout.addLayout(grid)
               
        self.dialogs = list()
        
        self.setLayout(outerLayout)

    def slider(self, var_list, action,  title = '', add = False):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
            sld = QSlider(Qt.Horizontal)
            sld.setFocusPolicy(Qt.StrongFocus)
            sld.setTickPosition(QSlider.TicksBothSides)
            sld.setTickInterval(10)
            sld.setSingleStep(1)
            sld.setValue(33)
            if add:
                sld.valueChanged[int].connect(var.update_slider_add)
            else:
                sld.valueChanged[int].connect(var.update_slider)
            sld.valueChanged[int].connect(action)
        
            label =QLabel()
            label.setText(var.name)
            Ggrid = QGridLayout()
            Ggrid.addWidget(var.num_lbl, 0,1)
            Ggrid.addWidget(label, 0,0)
            vbox.addLayout(Ggrid)
            vbox.addWidget(sld)

        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox
    
    def spinbox(self, var_list, action, title = '', one = False):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
        
        for var in var_list:
        
            Ggrid = QGridLayout()
            spinbox = QDoubleSpinBox()
            spinbox.setFixedSize(100,20)
            spinbox.setSuffix(var.unit)
            spinbox.setDecimals(3)
            spinbox.setRange(-np.sign(var.val)*var.val*var.fact*100,np.sign(var.val)*var.val*var.fact*100)
            if one:
                spinbox.setSingleStep(1*var.fact)
            else:                    
                spinbox.setSingleStep(var.val*var.fact/20)
            spinbox.setValue(var.val*var.fact)
            spinbox.valueChanged[float].connect(var.update_spinbox)
            spinbox.valueChanged.connect(action)
            label =QLabel()
            label.setText(var.name)
            Ggrid.addWidget(spinbox, 0, 1)
            Ggrid.addWidget(label, 0, 0)
            vbox.addLayout(Ggrid)
            
        
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox

    def slider_simple(self, var, action,  title = '', add = False):
        
        groupBox = QGroupBox(title)
        vbox = QVBoxLayout()
                    
        self.sld = QSlider(Qt.Horizontal)
        self.sld.setFocusPolicy(Qt.StrongFocus)
        self.sld.setTickPosition(QSlider.TicksBothSides)
        self.sld.setTickInterval(10)
        self.sld.setSingleStep(1)
        self.sld.setValue(33)
        self.sld.valueChanged[int].connect(var.update_slider_add)
        
        label =QLabel()
        label.setText(var.name)
        Ggrid = QGridLayout()
        Ggrid.addWidget(var.num_lbl, 0,1)
        Ggrid.addWidget(label, 0,0)
        vbox.addLayout(Ggrid)
        vbox.addWidget(self.sld)
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox    
    
    
    def init_plot(self):
        
        font = {
            'weight': 'normal',
            'size': 11
        }
        plt.rc('font', **font)
        
        self.ax = self.canvas.figure.subplots(nrows=2, ncols=2, gridspec_kw={'width_ratios': [2.5, 1]})
        
        self.ax[0,0].set_xlabel('time (ms)')
        self.ax[0,0].set_ylabel('lock-in singnal (mV)')
        self.line_r, = self.ax[0,0].plot([],[], color = 'tab:blue', label = 'R')
        self.line_x, = self.ax[0,0].plot([],[], color = 'tab:orange', label = 'X')
        self.line_y, = self.ax[0,0].plot([],[], color = 'tab:green', label = 'Y')
        self.ax[0,0].legend()
        
        self.ax[0,1].set_xlabel('lock-in singnal (mV)')
        self.ax[0,1].set_yticks([0,1,2])
        self.ax[0,1].set_yticklabels(['Y','X','R'])
        self.ax[0,1].set_ylim([-0.5, 2.5])
        self.ax[0,1].plot(np.zeros(100),np.linspace(-0.5,2.5,100),'--', color = 'black')
        self.Rbar = self.ax[0,1].barh([2],[1], color = 'tab:blue', height = 0.2)
        self.Xbar = self.ax[0,1].barh([1],[1], color = 'tab:orange', height = 0.2)
        self.Ybar = self.ax[0,1].barh([0],[1], color = 'tab:green', height = 0.2)
        
        
        self.ax[1,0].set_ylabel('V')
        self.ax[1,0].set_xlabel('$\mu$s')
        self.line_sgn, = self.ax[1,0].plot([],[], label = 'Sign', color = 'tab:blue')
        self.line_ref, = self.ax[1,0].plot([],[], label = 'Ref', color = 'darkblue')
        self.ax[1,0].legend()
        
        self.ax[1,1].set_ylabel('V')
        self.ax[1,1].set_xlabel('ns')
        self.line_sgn_raw, = self.ax[1,1].plot([],[], label = 'Sign', color = 'tab:blue')
        self.line_trg1, = self.ax[1,1].plot([],[],'--', linewidth = 0.75, label = 'window', color = 'tab:red')
        self.line_trg2, = self.ax[1,1].plot([],[],'--', linewidth = 0.75, color = 'tab:red')
        self.line_trg_lev, = self.ax[1,1].plot([],[], linewidth = 1, label = 'trig level', color = 'darkblue')
        self.ax[1,1].legend()


       
    def update_plot(self):
        
        if len(self.R)>0:
            t_lockin = np.arange(len(self.R))*len(self.ref)/self.f_sgn.val
            self.line_r.set_data(t_lockin,self.R*1e3)
            self.line_x.set_data(t_lockin,self.X*1e3)
            self.line_y.set_data(t_lockin,self.Y*1e3)
            self.ax[0,0].set_xlim([0, t_lockin[-1]])
            self.ax[0,0].set_ylim([-max(self.R)*1e3, max(self.R)*1.1*1e3])
        
        self.Rbar.remove()
        self.Xbar.remove()
        self.Ybar.remove()

        self.Rbar = self.ax[0,1].barh([2],[self.R[-1]*1e3], color = 'tab:blue', height = 0.2)
        self.Xbar = self.ax[0,1].barh([1],[self.X[-1]*1e3], color = 'tab:orange', height = 0.2)
        self.Ybar = self.ax[0,1].barh([0],[self.Y[-1]*1e3], color = 'tab:green', height = 0.2)
        self.ax[0,1].set_xlim([-max(self.R)*1.1*1e3, max(self.R)*1.1*1e3])
        
        t = np.arange(len(self.sign))/self.f_sgn.val*1e3
        self.line_sgn.set_data(t, self.sign)
        self.line_ref.set_data(t, self.ref*max(self.sign)) 
        self.ax[1,0].set_xlim([t[0], t[-1]])
        self.ax[1,0].set_ylim([0, max(self.sign)*1.1])
                
        self.canvas.draw()
        plt.pause(0.01)
        
    def update_plot_pulseshape(self):  
        
        
        limy = max(self.sign_rw)*1.1
        limy_m = min(self.sign_rw)*0.95
        t_raw = np.arange(-self.T_trg//2, self.T_trg//2)[1:]*1/self.f_sampl*1e6
        self.line_sgn_raw.set_data( t_raw, self.sign_rw)
        self.line_trg1.set_data(np.ones(100)*self.window_del.val*self.window_del.fact, np.linspace(limy_m,limy,100))
        self.line_trg2.set_data(np.ones(100)*(self.window_del.val+self.window_width.val)*self.window_width.fact, np.linspace(limy_m,limy,100))
        self.line_trg_lev.set_data(t_raw, np.ones(len(self.sign_rw))*self.trig_lev.val )
        self.ax[1,1].set_xlim([t_raw[0], t_raw[-1]])
        self.ax[1,1].set_ylim([limy_m, limy])
        
        self.canvas.draw()
        plt.pause(0.01)

                        
    def init_var(self, fsamp = 125e3, f_sgn = 87,
                 int_time = 2000, wndw_w = 1, wndw_d = -2,
                 trg_lev = 0.5):
        
        self.f_sampl = fsamp #kHz
        self.f_sgn = var('f sign', f_sgn, unit =  'MHz', fact = 1e-3)  #kHz         
        self.integr_time = var('integration time', int_time, unit = ' ms', fact = 1/self.f_sgn.val) #number of samples max = 2**14
        
        #window
        self.window_width = var('window width', wndw_w, unit = ' ns', fact = 1/self.f_sampl*1e6)                
        self.window_del = var('window delay', wndw_d, unit = ' ns', fact = 1/self.f_sampl*1e6) #ns
        
        
        #trigger
        self.trig_del = var('trigger delay', 1/self.f_sgn.val*1e3*0.2, unit = ' us', fact = 1)    #ns    
        self.trig_lev =var('trigger level', trg_lev, unit = ' V', fact = 1) #ns  
        
        #phase
        self.phase =var('phase', 0, unit = ' \u03C0', fact = 1/np.pi) 
        
        self.measure_en = False
    
    def start_acquisition(self):
        
        self.rdpt.set_scope( trig_lev= self.trig_lev.val, trig_delay = self.trig_del.val)
        self.rdpt.set_src([1,2])
#        self.rdpt.report_osc_stat()
#        print(self.window_del.val)
#        print(self.window_width.val)
        sign_raw = self.rdpt.quick_acq(src=1)
        sign_trig = np.squeeze( np.argwhere( np.diff( np.array(sign_raw>self.trig_lev.val, dtype = float) )>=1 ))
        
        # sign_tri=[0, 1000,1500]
        self.T_trg =  np.mean(np.diff(sign_trig))#1000

        self.f_sampl = self.T_trg*self.f_sgn.val # number of samples/ms
        self.sign_rw = sign_raw[sign_trig[1]-int(self.T_trg)//2:sign_trig[1]+int(self.T_trg)//2]
        self.update_plot_pulseshape()
        
        self.R, self.X, self.Y  = np.array([]), np.array([]), np.array([])                
        self.measure_toggle()

    def reset_acquisition(self):
        
        self.measure_en = False
        self.start_acquisition()
    
    def measure(self):
        
        self.sign = np.array([])
        self.rdpt.scope_arm()        
        self.sign, self.ref = self.rdpt.get_trigdata(Nint= self.window_width.val ,
                                                     N =  self.integr_time.val,
                                                     delay = self.window_del.val)
        
        self.ref = self.ref >= self.trig_lev.val
        r,x,y = self.rdpt.demod(self.sign, self.ref, phi = self.phase.val)
        self.R, self.X, self.Y  = np.append(self.R, r), np.append(self.X, x), np.append(self.Y, y)
        self.update_plot()    
            
    def measure_toggle(self):
        
        if self.measure_en:
            self.timer.stop()
            self.measure_en = False
        else:
            self.timer.start()
            self.measure_en = True

    def  autophase(self):
        self.measure_toggle()
        self.phase.val = np.pi+np.arctan(self.Y[-1]/self.X[-1])
        if np.isnan(self.phase.val):
            self.phase.val =0
        self.phase.val0 = self.phase.val
        self.phase.update_slider_add(0)
        self.sld.setValue(33)
        self.measure_toggle()

            
    def change_button(self):
        
        if self.measure_en:
            self.button.setText('Start')
        else:
            self.button.setText('Stop')
        
        self.start_acquisition()
        
        
    def generate(self):
        
        if self.rdpt.out_state[0]:
            self.rdpt.toggle_output()
            self.button_art.setText('Generate signal')
        else:
            
            self.trig_lev.val0, self.trig_lev.val = 0.2, 0.2
            self.f_sgn.val0, self.f_sgn.val0 = 950, 950
            self.integr_time.val0, self.integr_time.val = 200, 200
            
            self.trig_lev.update_slider(self.trig_lev.val0)
            self.f_sgn.update_spinbox(self.f_sgn.val0)
            
            self.rdpt.generate_continuous(1, self.f_sgn.val*1e3 , ampl= 2, wave_form = 'square')
            self.rdpt.set_wvf(1, offs= 2, mode = 'CONTINUOUS' )
            self.rdpt.generate_continuous(2, 50e3 , ampl= 2, wave_form = 'square')
            self.rdpt.set_wvf(2, offs= 2, mode = 'CONTINUOUS')
           
            self.rdpt.toggle_output(on_off = True)
            self.button_art.setText('Generating...')
            self.rdpt.set_src([1,2])

            # self.rdpt.quick_check(new_plt=True)
            
        
        
        
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    myApp = boxcar_lockin_app()
    myApp.resize(1050,950)
    myApp.show()
    sys.exit(app.exec_())