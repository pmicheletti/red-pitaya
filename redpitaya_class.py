# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 17:39:28 2021

@author: polen
"""

# -*- coding: utf-8 -*-
"""
Created on Nov 12 2019

@author: Paolo Micheletti
"""
from time import time, sleep
import redpitaya_scpi as scpi
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sgnl

class redpitaya:
    '''
    This class controls Mercury DC controller.

    tested models:  - 2400
    '''
    def __init__(self, addr = '169.254.248.16', plots=True):
        self.rp = scpi.scpi(addr)
        
        print('Connected to RedPitaya: %s'%self.rp.idn_q())
        self.out_state = np.array([False, False])
        self.wvf = ['sine','sine']
        self.trg_del_unit= 'ns'
        self.plots = plots
        self.gen_rst()
        if plots:
            self.graph()
        
    def read(self, string):        
        return self.rp.txrx_txt(string)
    
    def write(self, string):
        self.rp.tx_txt(string)
               
    def set_scope(self, dec =1,                 #decimation
                        avg = 'OFF',             #averaging
                        trig_src = 'EXT_PE',    #trigger source (external to default)    
                        trig_delay = 0,         #trigger delay (ns)
                        trig_delay_unit = 'ns',
                        trig_lev = 0.5        #trigger level (mV)
                        ):
        self.write('ACQ:RST')
        self.write('ACQ:AVG %s'%avg)
        self.write('ACQ:DEC %d'%dec)
         
        self.trig_src = trig_src
#        self.write('ACQ:TRIG %s'%trig_src)
        self.write('ACQ:TRIG:LEV %3.2f'%trig_lev)  
        self.trg_del_unit = trig_delay_unit
        if trig_delay_unit == 'ns':     
            self.rp.tx_txt('ACQ:TRIG:DLY:NS %3.1f'%trig_delay) 
        else:
            self.rp.tx_txt('ACQ:TRIG:DLY %3.1f'%trig_delay) 

    def get_sampl_rate(self):
        return self.read('ACQ:SRAT?')
        
    
    def report_osc_stat(self):
        print('Trigger:\n  \t state = %s'%self.read('ACQ:TRIG:STAT?') )
        print('\t level = %s mV'%self.read('ACQ:TRIG:LEV?') )
        if self.trg_del_unit == 'ns':     
            print('\t delay = %s ns'%self.read('ACQ:TRIG:DLY:NS?') )
        else:
            print('\t delay = %s samples'%self.read('ACQ:TRIG:DLY?') )
        print('\t decimation = %s'%self.read('ACQ:DEC?') )
        print('Source 1 :\n \t gain = %s'%self.read('ACQ:SOUR1:GAIN?') )       
        print('Source 2:\n \t gain = %s'%self.read('ACQ:SOUR2:GAIN?') )        

       
    def scope_arm(self): 
        self.write('ACQ:STOP')
        self.write('ACQ:START')
        self.write('ACQ:TRIG %s'%self.trig_src)
    
    def set_src(self, src, gain = 'HV' #HV or LV
                ):  
        if np.isscalar(src):
            self.rp.tx_txt('ACQ:SOUR%d:GAIN %s'%(src, gain))
        else:
            for s in src:
                self.rp.tx_txt('ACQ:SOUR%d:GAIN %s'%(s, gain))
        
    def start_acq(self):
        self.write('ACQ:START')
        self.write('DIG:PIN LED2,1')
        self.write('DIG:PIN LED4,1')
        self.write('DIG:PIN LED6,1')
        
    def stop_acq(self):
        self.write('ACQ:STOP')
        self.write('DIG:PIN LED2,0')
        self.write('DIG:PIN LED4,0')
        self.write('DIG:PIN LED6,0')


    def trig_stat(self):        
        return self.read('ACQ:TRIG:STAT?') == 'TD'
        
    def get_data(self, src, N=0):
        
        if N==0:
            self.rp.tx_txt('ACQ:SOUR%d:DATA?'%src)
        else:
            self.rp.tx_txt('ACQ:SOUR%d:DATA:OLD:N? %d'%(src, N))
            
        buff_string = self.rp.rx_txt()
        buff_string = buff_string.strip('{}\n\r').replace("  ", "").split(',')
        buff = np.array(list(map(float, buff_string)))/2
        
        return buff
   
    def get_trigdata(self, Nint=1, N = 2**14, delay = 0):
        

        self.rp.tx_txt('ACQ:TRIG:DATA:INT:N:DEL? %d, %d, %d'%(Nint, N, delay))
            
        buff_string = self.rp.rx_txt()
        buff_string = buff_string.strip().split('},{')
        
        buff_string1 = buff_string[0].strip('ERR!{').replace("  ", "").split(',')
        buff1 = np.array(list(map(float, buff_string1)))/2
        
        buff_string2 = buff_string[1].strip('}\n\r').replace("  ", "").split(',')
        buff2 = np.array(list(map(float, buff_string2)))/2
        
        return buff1, buff2
    
    def graph(self):
         
         self.fig, self.ax = plt.subplots()
         self.ax.set_ylabel('V')
         self.line1, = self.ax.plot([],[],'-*', label = 'S1', color = 'tab:blue')
         self.line2, = self.ax.plot([],[],'-*', label = 'S2', color = 'tab:orange')
        
    def update_plot(self, data1=[], data2=[], f1= 1, f2=1):
        
        if len(data1) > 0:
            self.line1.set_data(np.arange(len(data1))/f1,data1)
            lim1 = [min(data1)*(1-0.1*np.sign(min(data1))),max(data1)*1.1]
            self.ax.set_ylim(lim1)
            self.ax.set_xlim(0/f1, len(data1)/f1)
            if len(data2) > 0:
                  self.line2.set_data(np.arange(len(data2))/f2,data2)
                  lim2 = [min(data2)*(1-0.1*np.sign(min(data2))),max(data2)*1.1]
                  self.ax.set_ylim(min([lim1[0], lim2[0]]), max([lim1[1], lim2[1]]))
      
        self.fig.canvas.draw_idle()
        self.fig.tight_layout()
        plt.show()
        plt.pause(0.01)
        
    def demod(self, sgn, ref, phi = 0):
          
          n_sampls = len(sgn)  
          f0 = 1/np.mean(np.diff(np.squeeze(np.argwhere(np.diff(ref != 0)))))*len(ref)/n_sampls         
          n = np.arange(n_sampls)
          indx_fin = int(np.rint(n_sampls % (1/f0)))
          Cdemod = 1/(n_sampls-indx_fin)*np.sum(sgn[:-indx_fin]*np.exp(-1j*(np.pi*f0*n[:-indx_fin]+phi)))
          x = np.real(Cdemod)
          y = np.imag(Cdemod)
          r = np.abs(Cdemod)
          
          if self.plots:
              self.ax.set_title('R = %3.4f, X = %3.4f, Y = %3.4f'%(r,x,y))
              self.update_plot()
                    
          return r,x,y
        
    def close(self):
         self.rp.close()
               
         
    def generate_continuous(self, src, freq, ampl= 1, wave_form = 'sine', set_on = False):
        
        allowed_wvf = np.array(['sine', 'square','triangle','sawu', 'sawd', 'pwm', 'arbitrary', 'dc', 'dc_neg'])
        if not(any(wave_form == allowed_wvf)):
            print('unknown wave form! please select one among:')
            for wvf in allowed_wvf:
                print('- %s'%wvf)
            return
        else:
            self.wvf[src-1] = wave_form.upper()
        

        self.write('SOUR%d:FUNC '%src + self.wvf[src-1] )
        self.write('SOUR%d:FREQ:FIX %3.2f'%(src, freq))
        self.write('SOUR%d:VOLT %3.2f'%(src,ampl))
        
        if set_on:
            self.toggle_output(on_off = True)
    
    def gen_rst(self):
        self.write('GEN:RST')
        
        
    def toggle_output(self, on_off = None, src = [1,2]):
        
        if np.isscalar(src):
            src = [src]
        for s in src:    
            if on_off == None:
                self.out_state[s-1] = not(self.out_state[s-1])
            elif on_off:
                self.out_state[s-1] =True
            else:
                self.out_state[s-1] = False
            
            state_str = 'ON'*self.out_state[s-1] + 'OFF'*~(self.out_state[s-1])
            self.write('OUTPUT%d:STATE %s'%(s, state_str ))
            sleep(0.01)     
            print('Output %d switched %s'%(s, state_str ))
            
        return self.out_state
            
        

    def set_wvf(self, src, ducy = 0.5, offs = 0, mode = 'CONTINUOUS', 
                Ncyc= 1000,     #cycles/burst
                Nbur=10,        # numb of bursts 
                Per=1000        #burst period us
                ):
        
        if any(mode == np.array(['CONTINUOUS', 'BURST'])):
            self.write('SOUR%d:BURS:STAT %s'%(src, mode))
            if mode == 'BURST':
                self.write('SOUR%d:BURS:NCYC %d'%(src, Ncyc))
                self.write('SOUR%d:BURS:NOR %d'%(src, Ncyc))
                self.write('SOUR%d:BURS:INT:PER %d'%(src, Ncyc))
        else:
            print('unknown output mode! please select one among:')
            print('- CONTINUOUS ')
            print('- BURST ')

            
        if self.wvf[src-1]== 'PWM':    
            self.write('SOUR%d:DCYC %3.3f'%(src, ducy))
        self.write('SOUR%d:VOLT:OFFS %3.2f'%(src, offs))
     
        
    def quick_acq(self, src = '' ):
        self.start_acq()
        sleep(0.01) 
        if src == '':
            sign1 = self.get_data(1)
            sign2 = self.get_data(2)
            return sign1, sign2
        else:
            sign = self.get_data(src)
            return sign
        
    def quick_check(self, new_plt = False):   
        sign1, sign2 = self.quick_acq()
        if new_plt:
            plt.figure()
            plt.plot(sign1,'-*')
            plt.plot(sign2,'-*')
        else:
            self.update_plot(data1=sign1, data2=sign2)
            
        
    def fake_sgn(f, f_samp = 125e3, N = 16000, signA= 1, noiseA= 0, DuCy = 0.1):
       
        t = np.arange(N)/f_samp      
        x = signA*sgnl.square(2 * np.pi * f  * t, duty = DuCy)+np.random.rand(N)*noiseA+signA
        
        return x      