# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 18:37:53 2021

@author: polen
"""

import sys
sys.path.append(r'Z:\lab\meas\Paolo\Python\RedPitaya')
import numpy as np
from scipy.constants import e, hbar, h
import matplotlib.pyplot as plt
import time
from scipy import signal as sgnl

import redpitaya_class
plt.close('all')



t_int = 300 #ms
f_sgn = 1e3 #kHz
trig_del = 585 #ns
peak_width = 9 #ns

trig_lev_sign = 2
trig_lev = 0.5  #V


rdpt = redpitaya_class.redpitaya()
rdpt.set_scope(trig_lev = trig_lev, trig_delay = trig_del)
rdpt.set_src(1)
rdpt.set_src(2)
rdpt.report_osc_stat()



fig, ax = plt.subplots()
ax.set_ylabel('lock-in signal (V)')
ax.set_xlabel('time (ms)')
line_r, = ax.plot([],[], color = 'tab:blue')
line_x, = ax.plot([],[], color = 'tab:orange')
line_y, = ax.plot([],[], color = 'tab:green')

R, X, Y = np.array([]), np.array([]), np.array([])

#%%


while True:
    sign = np.array([])
    rdpt.scope_arm()        
    sign_raw = rdpt.get_data(1)
    ref = rdpt.get_data(2)>= trig_lev
    
    sign_trig = np.squeeze( np.argwhere( np.diff( np.array(sign_raw>trig_lev_sign, dtype = float) )>=1 ))
    f_sampl = np.mean(np.diff(sign_trig))*f_sgn # number of samples/ms
    
    delay = int(f_sampl*trig_del*1e-6)
    N = int(f_sampl*peak_width*1e-6)
    for i in range(len(sign_trig)):
        sign = np.append(sign, np.mean(sign_raw[sign_trig[i]+delay:sign_trig[i]+delay+N]) )
    
    rdpt.update_plot(sign, ref, f1 = f_sgn, f2 = f_sampl)
    r,x,y = rdpt.demod(sign, ref)
        
    
    R, X, Y  = np.append(R, r), np.append(X, x), np.append(Y, y)
    line_r.set_data(np.arange(len(R))*len(ref)/f_sampl,R)
    line_x.set_data(np.arange(len(R))*len(ref)/f_sampl,X)
    line_y.set_data(np.arange(len(R))*len(ref)/f_sampl,Y)
    
    ax.set_xlim([0, len(R)*len(ref)/f_sampl])
    ax.set_ylim([-max(R), max(R)*1.1])
    
    fig.canvas.draw_idle()
    fig.tight_layout()
    plt.show()
    plt.pause(0.01)

    
    
    
def fake_sgn(f_ref = 54, f_sign = 1e3, Nbuff = 16000, t_max = 0.12, signA= 10, noiseA= 0.5, DuCy = 0.1):
    
    
    t = np.linspace(0,t_max,Nbuff )
    
    ref = sgnl.square(2 * np.pi * f_ref  * t)+1
    sign = signA*sgnl.square(2 * np.pi * f_sign  * t, duty = DuCy)+np.random.rand(Nbuff)*noiseA+signA
    
    return sign, ref
    



    
    